import logo from './logo.svg';
import './App.css';
import cartoon from './mikasa.jpg';

function App() {
  return (
  <div>   
    <h1>Welcome to coursebooking batch 164</h1>
    <h5>This is our project in React</h5>
    <h6>Come visit our website</h6>
    <img src={cartoon} alt="image not found"/>
    <img src='./mikasa.jpg' alt="image not found"/>
    <h3>This is my favorite cartoon character</h3>
  </div>

  );
}

export default App;
